const MAX_VIDEOS = 300
const MAX_RELOADS = 8
const GET_OBJECT_TIMOUT = 20

async function sleep(ms){
    await new Promise(r => setTimeout(r, ms));
}

async function get_object(selector, kw={}){
    // Kwargs 
    index = kw.index | 0
    on_attempt = kw.on_attempt ||= () => {}
    has_value = kw.has_value

    attempts = 0
    await sleep(1000)

    obs = document.querySelectorAll(selector)

    while(obs[index] == null || (has_value !=null && obs[index][has_value] == null)){
        console.log('attemt' + attempts)
        on_attempt()
        await sleep(500)
        obs = document.querySelectorAll(selector)
        attempts++
        if (attempts*0.5 >= GET_OBJECT_TIMOUT) return null
    }
    return obs[index]
}


function click(selector){

}
async function scarp(){
    li = []
    i = 0
    
    while(li.length < MAX_VIDEOS){
        video = await get_object('#contents ytd-video-renderer #thumbnail', {index: i, on_attempt: ()=> {
            window.scrollTo(0, document.querySelector(".style-scope.ytd-two-column-browse-results-renderer#primary").scrollHeight);}
        });
        video.click();
        
        title_ob = await get_object('.title.style-scope.ytd-video-primary-info-renderer', {has_value: 'textContent'})
        title = title_ob.textContent

        share_button = await get_object('button#button[aria-label~="שיתוף"]');
        share_button.click();

        link_ob = await get_object('input#share-url')
        link = link_ob.value

        if (!li.some(o => o.link == link)) {
            li.push({'title': title, 'link': link });
        } else {console.log("Skippping: " + link)}
        
        console.log("Saving video: " + title + " id: " + i)   

        // going back to history 
        history_button = await get_object('a#endpoint[href~="/feed/history"]')
        history_button.click()
        await sleep(3000)
        i++;
    }
    return {'history': li}
}

function download_results(res) {
    var a = document.createElement("a");
    a.href = window.URL.createObjectURL(new Blob([res], {type: "text/plain"}));
    a.download = "Youtube-history.json";
    a.click();
}


async function run() {
    data = await scarp()
    download_results(JSON.stringify(data))
    console.log("Finnshed loading " + data['history'].length + " from history")
}
